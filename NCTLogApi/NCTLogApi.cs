﻿using System;
using System.Threading.Tasks;
using System.Net.Http;
using System.ComponentModel;
using System.Activities;
using System.Threading;
using Newtonsoft.Json;
using System.Text;
using System.Dynamic;

namespace NCTLogApi
{
    public class LogMessage
    {
        public string message_type { get; set; }
        public string message { get; set; }
    }

    public static class Extensions
    {
        public static Task<TResult> ToApm<TResult>(this Task<TResult> task, AsyncCallback callback, object state)
        {
            if (task.AsyncState == state)
            {
                if (callback != null)
                {
                    task.ContinueWith(delegate { callback(task); },
                                      CancellationToken.None, TaskContinuationOptions.None, TaskScheduler.Default);
                }
                return task;
            }

            var tcs = new TaskCompletionSource<TResult>(state);
            task.ContinueWith(obj =>
            {
                if (task.IsFaulted) tcs.TrySetException(task.Exception.InnerExceptions);
                else if (task.IsCanceled) tcs.TrySetCanceled();
                else tcs.TrySetResult(task.Result);

                if (callback != null) callback(tcs.Task);
            }, CancellationToken.None, TaskContinuationOptions.None, TaskScheduler.Default);
            return tcs.Task;
        }
    }

    public class PostMessage : AsyncCodeActivity { 

        [Category("Message")]
        [DisplayName("Message Type")]
        [RequiredArgument]
        public InArgument<String> Message_Type { get; set; }

        [Category("Message")]
        [RequiredArgument]
        [DisplayName("Message")]
        public InArgument<String> Message_Detail { get; set; }

        [Category("Token")]
        [RequiredArgument]
        [DisplayName("Token")]
        public InArgument<String> Token { get; set; }

        [Category("API")]
        [RequiredArgument]
        [DisplayName("Endpoint")]
        public InArgument<String> Endpoint { get; set; }

        [Category("Output")]
        [DisplayName("Response")]
        public OutArgument<HttpResponseMessage> ResponseMsg { get; set; }

        public TimeSpan Timeout { get; set; }
        public bool EnsureSuccessStatusCode { get; set; }

        protected HttpClient client = new HttpClient();

        protected LogMessage logMessage= null;

        public PostMessage()
        {
            this.Timeout = TimeSpan.FromSeconds(100);
            this.EnsureSuccessStatusCode = true;
            this.logMessage = new LogMessage();
           
        }

        public PostMessage(InArgument<string> message_Type, InArgument<string> message_Detail)
        {
            Message_Type = message_Type;
            Message_Detail = message_Detail;
        }

        protected override IAsyncResult BeginExecute(AsyncCodeActivityContext context, AsyncCallback callback, object state)
        {
            try
            {
                logMessage.message_type = Message_Type.Get(context);
                logMessage.message = Message_Detail.Get(context);

                Console.WriteLine("message: " + logMessage.message);
                Console.WriteLine("type: " + logMessage.message_type);
                var content = new StringContent(JsonConvert.SerializeObject(logMessage), Encoding.UTF8, "application/json");

                string token = Token.Get(context);
                string url = Endpoint.Get(context);
            
                Console.WriteLine("token :" + token);
                var httpClient = new HttpClient();
                var task =
                    httpClient.PostAsync(string.Format("{0}/api/logs/create?token={1}", url, token), content)
                    .ToApm(callback, state);
                task.ContinueWith(obj => httpClient.Dispose());
                return task;
            }
            catch(Exception e)
            {
                Console.WriteLine("error: " + e.Message);
                throw e;                
            }

        }

        protected override void EndExecute(AsyncCodeActivityContext context, IAsyncResult result)
        {
            try
            {
                HttpResponseMessage response = ((Task<HttpResponseMessage>)result).Result;
                if (this.EnsureSuccessStatusCode)
                    response.EnsureSuccessStatusCode();
                else if (response.IsSuccessStatusCode && string.Compare(response.Content.Headers.ContentType.MediaType, "application/json", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    response = response.Content.ReadAsAsync<HttpResponseMessage>().Result;
                }
                ResponseMsg.Set(context, response);
            }
            catch (AggregateException ae)
            {
                throw ae.InnerException;
            }
            catch (Exception ex)
            {
                throw ex;
            }       
        }

        protected override void Cancel(AsyncCodeActivityContext context)
        {
            base.Cancel(context);
        }
    }
}
